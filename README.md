# lambda

Lambda - Employees storage service

## Getting started

Lambda is containerized as a whole for convenience. Only 2 steps are enough to have everything set.

1) `docker pull ivoutsas/lambda`
2) `docker run -p 8080:8080 -p 8081:8081 --name lambda -d ivoutsas/lambda`

The use cases are pretty logical, common and self-explanatory. Accessing the GUI through `http://localhost:8081/` is enough to start goof around. For a deep and in detail look of the Rest API you can import the postman collection in your postman client and directly access the backend on `http://localhost:8080/`.

## Description
Lambda is a storage service for employees. It holds the different companies and the employees that are employeed to them. The Rest API covers a few different scenarios like add/delete a company, view the company information together with the employees list, as well as employees related scenarios like add/edit/delete and employee. The statistics endpoint is very simple yet, but it is scalable. For now, it just covers the average salary for a company and the summary salary.

Find the self-explanatory swagger specification in the respective directory!
![ScreenShot](/swagger/restApi.PNG)

## Installation
If you would like to contribute or just deploy Lambda through code for fun, you can simply just clone the repository. Having installed Node in your environment, you can then just `npm run serve -- --port 8081` to startup the Frontend from the lambdafe directory, and also from the project root directroy you can just `mvnw spring-boot:run` to have the backend started and accesible throught localhost:8080. 

## License
Copyright [yyyy] [name of copyright owner]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0


## Status
Lambda was developed for academic purposes trying to get in touch with the modern web tech stack. Among other, technologies that were used were Spring MVC, Spring Security, Spring Data, H2 as the in-memory database, Vue.js for the frontend with the help of Bootstrap. Docker was used to containerize the application. Log4j is the logger I used. The application is compiled with Java 11 because I found a handy docker image already existing with JDK11 and Node 14. Junit was used for the integration Tests. My IDE  was IntellIj. VCS = git!
