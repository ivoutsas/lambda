import axios from 'axios'

const EMPLOYEE_API_BASE_URL = "http://localhost:8080/employees"

class EmployeeService{
    getEmployees() {
        return axios.get(EMPLOYEE_API_BASE_URL);
    }
    getEmployee(id) {
        return axios.get(EMPLOYEE_API_BASE_URL + "/" + id);
    }
}

export default new EmployeeService()