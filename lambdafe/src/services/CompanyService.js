import axios from 'axios'

const COMPANY_API_BASE_URL = "http://localhost:8080/companies"
const STATS_API_BASE_URL = "http://localhost:8080/stats/companies"

class CompanyService{
    getCompanies() {
        return axios.get(COMPANY_API_BASE_URL);
    }
    getCompany(id) {
        return axios.get(COMPANY_API_BASE_URL+ "/" + id);
    }
    deleteCompany(id) {
        return axios.delete(COMPANY_API_BASE_URL+ "/" + id);
    }
    getSalaryStats(id, method) {
        return axios.get(STATS_API_BASE_URL+ "/" + id + "?salary=" + method);
    }
}

export default new CompanyService()