import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/companies",
      component: () => import("./components/CompanyList")
    },
    {
      path: "/companies/:id",
      component: () => import("./components/Company")
    },
    {
      path: "/addcompany",
      component: () => import("./components/AddCompany")
    },
    {
      path: "/employees",
      component: () => import("./components/EmployeeList")
    },
    {
    path: "/employees/:id",
    component: () => import("./components/Employee")
    },
    {
    path: "/addemployee",
    component: () => import("./components/AddEmployee")
    },
    {
      path: "/editemployee/:id",
      component: () => import("./components/EditEmployee")
    },
  ]
});