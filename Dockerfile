FROM timbru31/java-node:11-jdk-14

# install simple http server for serving static content
RUN npm install -g http-server

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY lambdafe lambdafe

# make the 'app' folder the current working directory
WORKDIR lambdafe

# install project dependencies
RUN npm install
RUN npm run build

EXPOSE 8080 8081

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar

COPY entrypoint.sh entrypoint.sh

RUN  chmod +X entrypoint.sh

EXPOSE 8080 8081

CMD ["./entrypoint.sh"]