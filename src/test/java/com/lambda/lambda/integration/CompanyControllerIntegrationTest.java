package com.lambda.lambda.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lambda.lambda.dto.CompanyDTO;
import com.lambda.lambda.model.Company;
import com.lambda.lambda.model.Employee;
import org.apache.log4j.Logger;
import org.assertj.core.api.AssertionsForInterfaceTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompanyControllerIntegrationTest {
    private static final Logger log = Logger.getLogger(CompanyControllerIntegrationTest.class);


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Simple Http GET request to access a specific company and assert the expected with the actual value persisted
     */
    @Test
    public void getCompany() {
        log.info("Creating the expected company.");
        Set<Employee> employeeSet = new HashSet<Employee>();
        Company tesla = new Company("Tesla");
        Employee elonMusk = new Employee("Elon", "Musk", "elonmusk@tesla.com", "San Jose, CA", 23000, tesla);
        Employee giannisVoutsas = new Employee("Ioannis", "Voutsas", "voutsasgx@gmail.com", "Thessaloniki, GR", 21000, tesla);
        employeeSet.add(elonMusk);
        employeeSet.add(giannisVoutsas);
        CompanyDTO companyDTOExp = new CompanyDTO(1,"Tesla", employeeSet);

        log.info("Getting the real company.");
        CompanyDTO companyDTOReal = this.restTemplate.getForObject("http://localhost:" + port + "/companies/1",
                CompanyDTO.class);

        log.info("Company persisted in the database is: " + companyDTOReal.toString());
        assertThat(companyDTOExp.getName()).isEqualTo(companyDTOReal.getName());
    }

    /**
     * Http GET request on a non-existent company id and asserting NOT_FOUND response
     * @throws URISyntaxException
     */
    @Test
    public void getCompany404() throws URISyntaxException {
        log.info("Creating the orphan path.");
        final String baseUrl = "http://localhost:" + port + "/companies/1926";
        URI uri = new URI(baseUrl);

        log.info("Sending the GET request...");
        ResponseEntity<CompanyDTO> result = this.restTemplate.getForEntity(uri, CompanyDTO.class);

        log.info("Response status code is: " + result.getStatusCode());
        AssertionsForInterfaceTypes.assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Http POST request containing the new company to persist in the database
     * @throws URISyntaxException
     */
    @Test
    public void postCompany() throws URISyntaxException {
        log.info("Create a company json.");
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode companyJson = mapper.createObjectNode();

        companyJson.put("name", "EBF");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(companyJson.toString(), headers);

        URI url = new URI("http://localhost:" + port + "/companies");

        log.info("Sending the POST request...");
        ResponseEntity<CompanyDTO> response = this.restTemplate.exchange(url, HttpMethod.POST, requestEntity, CompanyDTO.class);
        log.info("Response of POST is status code: " + response.getStatusCode() + " and company: " + response.getBody());

        assertThat(response.getBody().getName()).isEqualTo("EBF");

    }

    /**
     * Http GET request containing a JSON malformed Company asserting Bad Request
     * @throws URISyntaxException
     */
    @Test
    public void postCompany400() throws URISyntaxException {
        log.info("Create a malformed company json.");
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode companyJson = mapper.createObjectNode();

        companyJson.put("non existent field", "EBF");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(companyJson.toString(), headers);

        URI url = new URI("http://localhost:" + port + "/employees");

        log.info("Sending the POST request...");
        ResponseEntity<CompanyDTO> response = this.restTemplate.exchange(url, HttpMethod.POST, requestEntity, CompanyDTO.class);
        log.info("Response of POST is status code: " + response.getStatusCode());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    /**
     * Asserting a company exists and is deleted after an Http Delete request on the specific id
     * @throws URISyntaxException
     */
    @Test
    public void deleteCompany() throws URISyntaxException {

        final String baseUrl = "http://localhost:" + port + "/companies/3";
        URI uri = new URI(baseUrl);

        log.info("Verifying that object is persisted.");
        CompanyDTO companyOptReal = this.restTemplate.getForObject(baseUrl, CompanyDTO.class);

        assertThat(companyOptReal.getName()).isEqualTo("EBF");

        log.info("Sending the DELETE request...");
        restTemplate.delete(baseUrl);

        log.info("Verifying that object is deleted.");
        ResponseEntity<CompanyDTO> result = this.restTemplate.getForEntity(uri, CompanyDTO.class);
        AssertionsForInterfaceTypes.assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }

}
