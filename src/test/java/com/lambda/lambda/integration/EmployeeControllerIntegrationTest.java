package com.lambda.lambda.integration;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lambda.lambda.dto.EmployeeDTO;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.net.URI;
import java.net.URISyntaxException;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeControllerIntegrationTest {


    private static final Logger log = Logger.getLogger(EmployeeControllerIntegrationTest.class);

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Simple Http GET request to access a specific employee and assert the expected with the actual value persisted
     */
    @Test
    public void getEmployee() {

        log.info("Creating the expected employee.");
        EmployeeDTO employeeDTOExp = new EmployeeDTO(1, "Elon", "Musk", "elonmusk@tesla.com",
                "San Jose, CA", 23000, 1);

        log.info("Sending the GET request");
        EmployeeDTO employeeDTOreal = this.restTemplate.getForObject("http://localhost:" + port + "/employees/1",
                EmployeeDTO.class);
        log.info("Employee received is: " + employeeDTOreal.toString());

        assertThat(employeeDTOExp.getEmail()).isEqualTo(employeeDTOreal.getEmail());
        assertThat(employeeDTOExp.getSalary()).isEqualTo(employeeDTOreal.getSalary());
        assertThat(employeeDTOExp.getName()).isEqualTo(employeeDTOreal.getName());
        assertThat(employeeDTOExp.getAddress()).isEqualTo(employeeDTOreal.getAddress());
        assertThat(employeeDTOExp.getSurname()).isEqualTo(employeeDTOreal.getSurname());
        assertThat(employeeDTOExp.getId()).isEqualTo(employeeDTOreal.getId());
    }

    /**
     * Http GET request on a non-existent employee id and asserting NOT_FOUND response
     */
    @Test
    public void getEmployee404() throws URISyntaxException {

        log.info("Creating an non existent path.");
        final String baseUrl = "http://localhost:" + port + "/employees/1926";
        URI uri = new URI(baseUrl);

        log.info("Sending the GET request");
        ResponseEntity<EmployeeDTO> result = this.restTemplate.getForEntity(uri, EmployeeDTO.class);
        log.info("Response status code received is: " + result.getStatusCode());
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Http POST request containing the new employee to persist in the database
     * @throws URISyntaxException
     */
    @Test
    public void postEmployee() throws URISyntaxException {

        log.info("Creating a json employee.");
        final String name = "Linus";
        final String surname = "Torvalds";
        final String email = "linus@amazon.com";
        final String address = "Thessaloniki, GR";
        final Integer salary = 20000;
        final Integer company_id = 2;

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode employeeJson = mapper.createObjectNode();

        employeeJson.put("name", name);
        employeeJson.put("surname", surname);
        employeeJson.put("email", email);
        employeeJson.put("address", address);
        employeeJson.put("salary", salary);
        employeeJson.put("company_id", company_id);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<String>(employeeJson.toString(), headers);

        URI url = new URI("http://localhost:" + port + "/employees");

        log.info("Post employee.");
        ResponseEntity<EmployeeDTO> response = this.restTemplate.exchange(url, HttpMethod.POST, requestEntity, EmployeeDTO.class);
        log.info("Response from Http POST request is: " + response.getStatusCode() + " with data: " + response.getBody());

        assertThat(response.getBody().getName()).isEqualTo(name);
        assertThat(response.getBody().getSurname()).isEqualTo(surname);
        assertThat(response.getBody().getEmail()).isEqualTo(email);
        assertThat(response.getBody().getAddress()).isEqualTo(address);
        assertThat(response.getBody().getSalary()).isEqualTo(salary);
        assertThat(response.getBody().getCompany_id()).isEqualTo(company_id);

    }

    /**
     * Http GET request containing a JSON malformed Employee asserting Bad Request
     * @throws URISyntaxException
     */
    @Test
    public void postEmployee400() throws URISyntaxException {

        log.info("Creating a json with malformed employee.");
        final String name = "Linus";
        final String surname = "Torvalds";
        final String email = "linus@amazon.com";
        final String address = "Thessaloniki, GR";
        final String salary = "String Instead of Integer";
        final Integer company_id = 2;

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode employee = mapper.createObjectNode();

        employee.put("name", name);
        employee.put("surname", surname);
        employee.put("email", email);
        employee.put("address", address);
        employee.put("salary", salary);
        employee.put("company_id", company_id);

        String jsonString = employee.toString();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<String>(jsonString, headers);

        URI url = new URI("http://localhost:" + port + "/employees");

        log.info("Post employee.");
        ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        log.info("Response from Http POST request is: " + response.getStatusCode());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    /**
     * Asserting an employee exists and is deleted after an Http Delete request on the specific id
     * @throws URISyntaxException
     */
    @Test
    public void deleteEmployee() throws URISyntaxException {

        log.info("Verifying that object is persisted.");
        EmployeeDTO employeeDTOreal = this.restTemplate.getForObject("http://localhost:" + port + "/employees/3",
                EmployeeDTO.class);

        assertThat(employeeDTOreal.getEmail()).isEqualTo("jeffbezos@amazon.com");

        log.info("Delete employee.");
        restTemplate.delete("http://localhost:" + port + "/employees/3");

        final String baseUrl = "http://localhost:" + port + "/employees/3";
        URI uri = new URI(baseUrl);

        log.info("Verifying that object is deleted.");
        ResponseEntity<EmployeeDTO> result = this.restTemplate.getForEntity(uri, EmployeeDTO.class);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Asserting an employee exists and is patched after an Http Path request with the edit payload as Json
     * @throws URISyntaxException
     */
    @Test()
    @Disabled
    public void patchEmployee() throws URISyntaxException {
        log.info("Creating an employee");
        EmployeeDTO employeeDTOExp = new EmployeeDTO(1, "Elon", "Musk", "elonmusk@tesla.com",
                "San Jose, CA", 23000, 1);
        EmployeeDTO employeeDTOreal = this.restTemplate.getForObject("http://localhost:" + port + "/employees/1",
                EmployeeDTO.class);
        log.info("The persisted employee is:" + employeeDTOreal.toString());

        assertThat(employeeDTOExp.getEmail()).isEqualTo(employeeDTOreal.getEmail());
        assertThat(employeeDTOExp.getSalary()).isEqualTo(employeeDTOreal.getSalary());
        assertThat(employeeDTOExp.getName()).isEqualTo(employeeDTOreal.getName());
        assertThat(employeeDTOExp.getAddress()).isEqualTo(employeeDTOreal.getAddress());
        assertThat(employeeDTOExp.getSurname()).isEqualTo(employeeDTOreal.getSurname());
        assertThat(employeeDTOExp.getId()).isEqualTo(employeeDTOreal.getId());

        employeeDTOExp.setSalary(25000);
        employeeDTOExp.setEmail("elunmust@spacex.com");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        log.info("Creating the patch information.");
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode empoloyeePatchJson = mapper.createObjectNode();

        empoloyeePatchJson.put("salary", 25000);
        empoloyeePatchJson.put("email", "elunmust@spacex.com");


        HttpEntity<String> requestEntity = new HttpEntity<String>(empoloyeePatchJson.toString(), headers);

        URI url = new URI("http://localhost:" + port + "/employees/1");

        log.info("Sending the patch request.");
        this.restTemplate.patchForObject(url, requestEntity, EmployeeDTO.class);
        log.info("Employee is patched.");

        employeeDTOreal = this.restTemplate.getForObject("http://localhost:" + port + "/employees/1",
                EmployeeDTO.class);
        log.info("The updated employee is: " + employeeDTOreal.toString());

        assertThat(employeeDTOExp.getEmail()).isEqualTo(employeeDTOreal.getEmail());
        assertThat(employeeDTOExp.getSalary()).isEqualTo(employeeDTOreal.getSalary());
        assertThat(employeeDTOExp.getName()).isEqualTo(employeeDTOreal.getName());
        assertThat(employeeDTOExp.getAddress()).isEqualTo(employeeDTOreal.getAddress());
        assertThat(employeeDTOExp.getSurname()).isEqualTo(employeeDTOreal.getSurname());
        assertThat(employeeDTOExp.getId()).isEqualTo(employeeDTOreal.getId());

    }
}
