package com.lambda.lambda.integration;

import com.lambda.lambda.dto.CompanyDTO;
import com.lambda.lambda.dto.SalaryDTO;
import com.lambda.lambda.model.Company;
import com.lambda.lambda.model.Employee;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatsControllerIntegrationTest {
    private static final Logger log = Logger.getLogger(StatsControllerIntegrationTest.class);


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     *  Assert the result of the company average salaries
     */
    @Test
    public void getAverageSalary() {

        assertSalaryStats("average", 22000.0f);

    }

    /**
     *  Assert the result of the company sum salaries
     */
    @Test
    public void getSumSalary() throws URISyntaxException {

        assertSalaryStats("sum", 44000.0f);

    }

    private void assertSalaryStats(String salary, Float value) {
        log.info("Creating the employees set to assert.");
        Set<Employee> employeeSet = new HashSet<Employee>();
        Company tesla = new Company("Tesla");
        Employee elonMusk = new Employee("Elon", "Musk", "elonmusk@tesla.com", "San Jose, CA", 23000, tesla);
        Employee giannisVoutsas = new Employee("Ioannis", "Voutsas", "voutsasgx@gmail.com", "Thessaloniki, GR", 21000, tesla);
        employeeSet.add(elonMusk);
        employeeSet.add(giannisVoutsas);
        CompanyDTO companyDTOExp = new CompanyDTO(1, "Tesla", employeeSet);

        log.info("Getting the actual employee set.");
        CompanyDTO companyDTOReal = this.restTemplate.getForObject("http://localhost:" + port + "/companies/1", CompanyDTO.class);

        assertThat(companyDTOExp.getName()).isEqualTo(companyDTOReal.getName());

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        String urlTemplate = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/stats/companies/1")
                .queryParam("salary", "{salary}")
                .encode()
                .toUriString();

        Map<String, String> params = new HashMap<>();
        params.put("salary", salary);

        log.info("Getting the statistics.");
        HttpEntity<SalaryDTO> response = this.restTemplate.exchange(urlTemplate, HttpMethod.GET, entity, SalaryDTO.class, params);

        log.info("Asserting correct result.");
        assertThat(response.getBody().getSalary()).isEqualTo(value);
    }


}
