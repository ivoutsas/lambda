package com.lambda.lambda.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.lambda.lambda.dto.EmployeeDTO;

import javax.persistence.*;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column
    private String surname;

    @Column(unique = true)
    private String email;

    @Column
    private String address;

    @Column
    private Integer salary;

    @ManyToOne
    @JoinColumn(name = "company_id")
    @JsonBackReference
    private Company company;

    public Employee() {

    }

    public Employee(EmployeeDTO employeeDTO) {
        this.setName(employeeDTO.getName());
        this.setSurname(employeeDTO.getSurname());
        this.setAddress(employeeDTO.getAddress());
        this.setEmail(employeeDTO.getEmail());
        this.setSalary(employeeDTO.getSalary());
        this.setCompany(company);
    }


    public Employee(String name, String surname, String email, String address, Integer salary, Company company) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.salary = salary;
        this.company = company;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSalary() {
        return this.salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Company getCompany() {
        return this.company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
