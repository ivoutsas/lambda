package com.lambda.lambda.repository;

import com.lambda.lambda.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    List<Employee> findAllByCompany_Id(Integer companyId);

    Employee findByEmail(String email);

}