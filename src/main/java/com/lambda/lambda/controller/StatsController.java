package com.lambda.lambda.controller;

import com.lambda.lambda.dto.SalaryDTO;
import com.lambda.lambda.service.CompanyService;
import com.lambda.lambda.service.StatsService;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/stats/")
@CrossOrigin("http://localhost:8081")
public class StatsController {

    private static final Logger log = Logger.getLogger(StatsController.class);

    private final StatsService statsService;
    private final CompanyService companyService;

    public StatsController(StatsService statsService, CompanyService companyService) {
        this.statsService = statsService;
        this.companyService = companyService;
    }

    @GetMapping("/companies/{id}")
    public ResponseEntity<SalaryDTO> getCompanyById(@PathVariable Integer id, @RequestParam String salary) {
        if (this.companyService.getCompanyById(id).isPresent()) {
            return ResponseEntity.of(this.statsService.getSalaryStats(id, salary));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}