package com.lambda.lambda.controller;

import com.lambda.lambda.dto.EmployeeDTO;
import com.lambda.lambda.model.Company;
import com.lambda.lambda.model.Employee;
import com.lambda.lambda.service.CompanyService;
import com.lambda.lambda.service.EmployeeService;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
@CrossOrigin("http://localhost:8081")
public class EmployeeController {

    private static final Logger log = Logger.getLogger(EmployeeController.class);

    private static final ModelMapper modelMapper = new ModelMapper();

    private final EmployeeService employeeService;
    private final CompanyService companyService;

    PropertyMap<EmployeeDTO, Employee> employeeMapping = new PropertyMap<EmployeeDTO, Employee>() {
        protected void configure() {
            map().getCompany().setId(source.getCompany_id());
        }
    };

    PropertyMap<Employee, EmployeeDTO> employeeDTOMapping = new PropertyMap<Employee, EmployeeDTO>() {
        protected void configure() {
            map().setCompany_id(source.getCompany().getId());
        }
    };

    public EmployeeController(EmployeeService employeeService, CompanyService companyService) {
        modelMapper.addMappings(employeeMapping);
        modelMapper.addMappings(employeeDTOMapping);
        this.employeeService = employeeService;
        this.companyService = companyService;
    }

    @GetMapping()
    public ResponseEntity<List<EmployeeDTO>> getFullEmployeesCatalog() {
        Optional<List<EmployeeDTO>> employeeDTOListOpt = employeeService.getFullEmployeesCatalog() //
                .map(list -> list.stream().map(emp -> modelMapper.map(emp, EmployeeDTO.class)).collect(Collectors.toList()));
        return ResponseEntity.of(employeeDTOListOpt);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable Integer id) {
        Optional<EmployeeDTO> employeeDTOOpt = this.employeeService.getEmployeeById(id).map(emp -> modelMapper.map(emp, EmployeeDTO.class));
        return ResponseEntity.of(employeeDTOOpt);
    }

    @PostMapping()
    public ResponseEntity<EmployeeDTO> addEmployee(@RequestBody EmployeeDTO employeeDTO) {
        log.debug("New request received: " + employeeDTO.toString());
        if (employeeDTO.getCompany_id() != null) {
            Optional<Company> companyOpt = this.companyService.getCompanyById(employeeDTO.getCompany_id());
            if (validEmployee(employeeDTO, companyOpt)) {
                if (employeeExists(employeeDTO)) {
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                } else {
                    Optional<Employee> employeeOpt = this.employeeService.addEmployee(modelMapper.map(employeeDTO, Employee.class));
                    Optional<EmployeeDTO> employeeDTOOpt = employeeOpt.map(emp -> modelMapper.map(emp, EmployeeDTO.class));
                    return ResponseEntity.of(employeeDTOOpt);
                }
            } else {
                return ResponseEntity.badRequest().build();
            }
        } else {
            return ResponseEntity.badRequest().build();
        }

    }

    @PatchMapping("/{id}")
    public ResponseEntity<EmployeeDTO> updateEmployeeById(@PathVariable Integer id, @RequestBody EmployeeDTO employeeDTO) {

        Optional<Employee> employeeOpt = this.employeeService.updateEmployeeById(id, modelMapper.map(employeeDTO, Employee.class));
        Optional<EmployeeDTO> employeeDTOOpt = employeeOpt.map(emp -> modelMapper.map(emp, EmployeeDTO.class));
        return ResponseEntity.of(employeeDTOOpt);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployeeById(@PathVariable Integer id) {
        try {
            this.employeeService.deleteEmployeeById(id);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException ex) {
            log.error(ex.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    private boolean employeeExists(EmployeeDTO employeeDTO) {
        return this.employeeService.getEmployeeByEmail(employeeDTO.getEmail()).isPresent();
    }

    private boolean validEmployee(EmployeeDTO employeeDTO, Optional<Company> companyOpt) {
        return employeeDTO.getName() != null && employeeDTO.getSurname() != null //
                && employeeDTO.getAddress() != null && employeeDTO.getEmail() != null //
                && employeeDTO.getSalary() != null && employeeDTO.getSalary() >= 0 //
                && companyOpt.isPresent();
    }

}
