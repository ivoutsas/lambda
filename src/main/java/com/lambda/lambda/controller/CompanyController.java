package com.lambda.lambda.controller;

import com.lambda.lambda.dto.CompanyDTO;
import com.lambda.lambda.model.Company;
import com.lambda.lambda.service.CompanyService;
import com.lambda.lambda.service.EmployeeService;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
@CrossOrigin("http://localhost:8081")
public class CompanyController {

    private static final Logger log = Logger.getLogger(CompanyController.class);

    private static final ModelMapper modelMapper = new ModelMapper();
    private final CompanyService companyService;
    public CompanyController(CompanyService companyService, EmployeeService employeeService) {
        this.companyService = companyService;
    }

    @GetMapping()
    public ResponseEntity<List<CompanyDTO>> getFullCompaniesCatalog() {
        Optional<List<CompanyDTO>> companyDTOOpt = companyService.getFullEmployeesCatalog().map(list -> list.stream() //
                .map(comp -> modelMapper.map(comp, CompanyDTO.class)).collect(Collectors.toList()));
        return ResponseEntity.of(companyDTOOpt);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CompanyDTO> getCompanyById(@PathVariable Integer id) {
        Optional<CompanyDTO> companyDTOOpt = this.companyService.getCompanyById(id).map(comp -> modelMapper.map(comp, CompanyDTO.class));
        return ResponseEntity.of(companyDTOOpt);
    }

    @PostMapping()
    public ResponseEntity<CompanyDTO> addCompany(@RequestBody CompanyDTO companyDTO) {
        log.debug("New company to add: " + companyDTO.getName());
        if (this.companyService.getCompanyByName(companyDTO.getName()).isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } else {
            Company company = modelMapper.map(companyDTO, Company.class);
            Optional<CompanyDTO> companyDTOOpt = this.companyService.addCompany(company).map(comp -> modelMapper.map(comp, CompanyDTO.class));
            return ResponseEntity.of(companyDTOOpt);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCompanyById(@PathVariable Integer id) {
        try {
            Optional<Company> companyOpt = this.companyService.getCompanyById(id);
            if (companyOpt.isPresent() && companyOpt.get().getEmployees().size() != 0) {
                return ResponseEntity.badRequest().build();
            } else {
                this.companyService.deleteCompanyById(id);
                return ResponseEntity.ok().build();
            }
        } catch (EmptyResultDataAccessException ex) {
            log.error(ex.getMessage());
            return ResponseEntity.notFound().build();
        }
    }


}