package com.lambda.lambda.service;

import com.lambda.lambda.dto.SalaryDTO;
import com.lambda.lambda.repository.CompanyRepository;
import com.lambda.lambda.model.Company;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private static final Logger log = Logger.getLogger(CompanyService.class);

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public Optional<List<Company>> getFullEmployeesCatalog() {

        return Optional.of(this.companyRepository.findAll());
    }

    public Optional<Company> addCompany(Company company) {
        company.setName(company.getName());
        return Optional.of(this.companyRepository.save(company));
    }

    public Optional<Company> getCompanyById(Integer id) {
        return this.companyRepository.findById(id);
    }

    public Optional<Company> getCompanyByName(String name) {
        return Optional.ofNullable(this.companyRepository.findByName(name));
    }

    public void deleteCompanyById(Integer id) {
        this.companyRepository.deleteById(id);
    }

}
