package com.lambda.lambda.service;

import com.lambda.lambda.controller.EmployeeController;
import com.lambda.lambda.model.Employee;
import com.lambda.lambda.repository.CompanyRepository;
import com.lambda.lambda.repository.EmployeeRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    private static final Logger log = Logger.getLogger(EmployeeController.class);

    private final EmployeeRepository employeeRepository;
    private final CompanyRepository companyRepository;


    public EmployeeService(EmployeeRepository employeeRepository, CompanyRepository companyRepository) {
        this.employeeRepository = employeeRepository;
        this.companyRepository = companyRepository;
    }

    public Optional<List<Employee>> getFullEmployeesCatalog() {

        return Optional.of(this.employeeRepository.findAll());
    }

    public Optional<Employee> getEmployeeById(Integer id) {
        return this.employeeRepository.findById(id);
    }

    public Optional<Employee> addEmployee(Employee employee) {
        return Optional.of(this.employeeRepository.save(employee));
    }

    public Optional<Employee> getEmployeeByEmail(String email) {
        return Optional.ofNullable(this.employeeRepository.findByEmail(email));
    }

    public Optional<Employee> updateEmployeeById(Integer id, Employee employeeUpdatePayload) {
        Optional<Employee> persistedEmployeeOpt = this.employeeRepository.findById(id);
        if (!persistedEmployeeOpt.isPresent()) {
            return Optional.empty();
        } else {
            Employee emp = persistedEmployeeOpt.get();
            if (employeeUpdatePayload.getName() != null && !employeeUpdatePayload.getName().equals("")) {
                emp.setName(employeeUpdatePayload.getName());
            }
            if (employeeUpdatePayload.getSurname() != null && !employeeUpdatePayload.getSurname().equals("")) {
                emp.setSurname(employeeUpdatePayload.getSurname());
            }
            if (employeeUpdatePayload.getEmail() != null && !employeeUpdatePayload.getEmail().equals("")) {
                emp.setEmail(employeeUpdatePayload.getEmail());
            }
            if (employeeUpdatePayload.getAddress() != null && !employeeUpdatePayload.getAddress().equals("")) {
                emp.setAddress(employeeUpdatePayload.getAddress());
            }
            if (employeeUpdatePayload.getSalary() != null && employeeUpdatePayload.getSalary() > 0) {
                emp.setSalary(employeeUpdatePayload.getSalary());
            }
            if (employeeUpdatePayload.getCompany() != null && employeeUpdatePayload.getCompany().getId() > 0) {
                if (this.companyRepository.findById(employeeUpdatePayload.getCompany().getId()).isPresent() ) {
                    emp.setCompany(employeeUpdatePayload.getCompany());
                } else {
                    return Optional.empty();
                }
            }
            return Optional.of(this.employeeRepository.save(emp));
        }
    }

    public void deleteEmployeeById(Integer id) {
        this.employeeRepository.deleteById(id);
    }

}
