package com.lambda.lambda.service;

import com.lambda.lambda.dto.SalaryDTO;
import com.lambda.lambda.model.Employee;
import com.lambda.lambda.repository.EmployeeRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StatsService {

    private static final Logger log = Logger.getLogger(StatsService.class);

    private final EmployeeRepository employeeRepository;

    public StatsService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Optional<SalaryDTO> getSalaryStats(Integer id, String salary) {
        if (salary.equalsIgnoreCase(SalaryFunction.AVERAGE.toString())) {
            List<Employee> employees = employeeRepository.findAllByCompany_Id(id);
            if (employees.size() <= 0) {
                return Optional.empty();
            } else {
                int sum = employees.stream().mapToInt(Employee::getSalary).sum();
                Float average = (float) sum / employees.size();
                return Optional.of(new SalaryDTO(average));
            }
        } else if (salary.equalsIgnoreCase(SalaryFunction.SUM.toString())) {
            List<Employee> employees = employeeRepository.findAllByCompany_Id(id);
            float sum = employees.stream().mapToInt(Employee::getSalary).sum();
            return Optional.of(new SalaryDTO(sum));
        } else {
            return Optional.empty();
        }
    }

    public enum SalaryFunction {
        AVERAGE, SUM;
    }

}
