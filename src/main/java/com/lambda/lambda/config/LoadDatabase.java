//package com.lambda.lambda.config;
//
//import com.lambda.lambda.model.User;
//import com.lambda.lambda.repository.UserRepository;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//
//@Configuration
//class LoadDatabase {
//
//    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
//
//    @Bean
//    CommandLineRunner initDatabase(UserRepository repository) {
//        return args -> {
//            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//            log.info("Preloading " + repository.save(new User("admin", passwordEncoder.encode("admin"), User.Role.ADMIN, true)));
//        };
//    }
//
//}