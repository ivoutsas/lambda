//package com.lambda.lambda.config;
//
//import com.lambda.lambda.model.User;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//
//import javax.sql.DataSource;
//
//@Configuration
//@EnableWebSecurity
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    private DataSource dataSource;
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.cors().and().formLogin().disable()
//                .authorizeRequests().antMatchers("/", "/h2/**").permitAll()
//                .and().csrf().ignoringAntMatchers("/h2/**")
//                .and().headers().frameOptions().sameOrigin().and().authorizeRequests()
//                .anyRequest().hasAuthority(User.Role.ADMIN.toString())
//                .and()
//                .logout()
//                .permitAll().and().csrf().disable().httpBasic();
//
//    }
//
//    @Autowired
//    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication().passwordEncoder(new BCryptPasswordEncoder())
//                .dataSource(dataSource)
//                .usersByUsernameQuery("select username, password, enabled from user where username=?")
//                .authoritiesByUsernameQuery("select username, role from user where username=?");
//    }
//
//}