package com.lambda.lambda.dto;

import lombok.*;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SalaryDTO {

    @Getter
    @Setter
    private Float salary;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SalaryDTO salaryDTO = (SalaryDTO) o;
        return Objects.equals(salary, salaryDTO.salary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(salary);
    }

    @Override
    public String toString() {
        return "SalaryDTO{" +
                "salary=" + salary +
                '}';
    }
}
