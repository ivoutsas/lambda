package com.lambda.lambda.dto;

import lombok.*;
import org.springframework.lang.NonNull;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EmployeeDTO {

    @NonNull
    @Getter
    @Setter
    private Integer id;

    @NonNull
    @Getter
    @Setter
    private String name;

    @NonNull
    @Getter
    @Setter
    private String surname;

    @NonNull
    @Getter
    @Setter
    private String email;

    @NonNull
    @Getter
    @Setter
    private String address;

    @NonNull
    @Getter
    @Setter
    private Integer salary;

    @NonNull
    @Getter
    @Setter
    private Integer company_id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeDTO that = (EmployeeDTO) o;
        return id.equals(that.id) && name.equals(that.name) && surname.equals(that.surname) && email.equals(that.email) && address.equals(that.address) && salary.equals(that.salary) && company_id.equals(that.company_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, email, address, salary, company_id);
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", salary=" + salary +
                ", company_id=" + company_id +
                '}';
    }
}
