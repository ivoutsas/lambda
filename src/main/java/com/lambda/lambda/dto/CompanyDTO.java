package com.lambda.lambda.dto;


import com.lambda.lambda.model.Employee;
import lombok.*;
import org.springframework.lang.NonNull;

import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CompanyDTO {

    @NonNull
    @Getter
    @Setter
    private Integer id;

    @NonNull
    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private Set<Employee> employees;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyDTO that = (CompanyDTO) o;
        return id.equals(that.id) && name.equals(that.name) && Objects.equals(employees, that.employees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, employees);
    }

    @Override
    public String toString() {
        return "CompanyDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", employees=" + employees +
                '}';
    }
}
